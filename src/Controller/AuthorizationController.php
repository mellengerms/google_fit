<?php

namespace Drupal\google_fit\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Google_Client;
use Google_Service_Fitness;

/**
 * Class AuthorizationController.
 */
class AuthorizationController extends ControllerBase {

  /**
   * Authentificate.
   *
   * @return string
   *   Return Hello string.
   */
  public function authentificate() {
    $client = $this->getGoolgeClient();
    $service = new Google_Service_Fitness($client);


    /************************************************
    If we have a code back from the OAuth 2.0 flow,
    we need to exchange that with the authenticate()
    function. We store the resultant access token
    bundle in the session, and redirect to ourself.
    ************************************************/
    if (isset($_GET['code'])) {
      $client->authenticate($_GET['code']);
        $_SESSION['access_token'] = $client->getAccessToken();
        $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
        header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
        echo "EXCHANGE";
    }

    /************************************************
    If we have an access token, we can make
    requests, else we generate an authentication URL.
    ************************************************/
    if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
        $client->setAccessToken($_SESSION['access_token']);
        echo "GOT IT";

        $timezone = "GMT+0100";
        $today = date("Y-m-d");
        $endTime = strtotime($today.' 00:00:00 '.$timezone);
        $startTime = strtotime('-7 days', $endTime);

        $step_count = $this->getStepCount($service, $startTime, $endTime);

        print("STEP: " . $step_count . "<br />");
    } else {
        $authUrl = $client->createAuthUrl();
    }

    /************************************************
    If we're signed in and have a request to shorten
    a URL, then we create a new URL object, set the
    unshortened URL, and call the 'insert' method on
    the 'url' resource. Note that we re-store the
    access_token bundle, just in case anything
    changed during the request - the main thing that
    might happen here is the access token itself is
    refreshed if the application has offline access.
    ************************************************/
    if ($client->getAccessToken() && isset($_GET['url'])) {
        $_SESSION['access_token'] = $client->getAccessToken();
    }

    if (isset($authUrl)) {
      $build['connect'] = [
        '#title' => $this
          ->t('Connect to Google fit'),
        '#type' => 'link',
        '#url' => Url::fromUri($authUrl),
      ];
    }
    else {
      $build = [
        '#type' => 'markup',
        '#markup' => $this->t('Implement method: authentificate')
      ];
    }

    return $build;
  }

  public function getGoolgeClient() {
    $config = \Drupal::config('google_fit.settings');
    $client_id = $config->get('client_id');
    $client_secret = $config->get('client_secret');
    $options = ['absolute' => TRUE];
    $url = Url::fromRoute('google_fit.authorization_controller_authentificate', [], $options);
    $redirect_uri = $url->toString();

    $client = new Google_Client();
    $client->setApplicationName('google-fit');
    $client->setAccessType('online');
    $client->setApprovalPrompt("auto");
    $client->setClientId($client_id);
    $client->setClientSecret($client_secret);
    $client->setRedirectUri($redirect_uri);

    $client->addScope(Google_Service_Fitness::FITNESS_ACTIVITY_READ);

    return $client;
  }

  public function getStepCount($service, $startTime, $endTime) {
    $dataSources = $service->users_dataSources;
    $dataSets = $service->users_dataSources_datasets;

    $listDataSources = $dataSources->listUsersDataSources("me");

    while ($listDataSources->valid()) {
      $dataSourceItem = $listDataSources->next();
      if ($dataSourceItem['dataType']['name'] == "com.google.step_count.delta") {
        $dataStreamId = $dataSourceItem['dataStreamId'];
        $listDatasets = $dataSets->get("me", $dataStreamId, $startTime . '000000000' . '-' . $endTime . '000000000');

        $step_count = 0;
        while ($listDatasets->valid()) {
          $dataSet = $listDatasets->next();
          $dataSetValues = $dataSet['value'];

          if ($dataSetValues && is_array($dataSetValues)) {
            foreach ($dataSetValues as $dataSetValue) {
              $step_count += $dataSetValue['intVal'];
            }
          }
        }
      };
    }
    return $step_count;
  }

}
